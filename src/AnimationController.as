package  
{
	import flash.display.Sprite;
	/**
	 * ...
	 * @author Roman Komissarov
	 */
	public class AnimationController extends Sprite
	{
		
		private static var _i:AnimationController;
		
		public function AnimationController() 
		{
		}
		
		public function get i():AnimationController {
			
			if (!_i) {
				_i = new AnimationController();
			}
			
			return _i;
		}
		
		
	}

}
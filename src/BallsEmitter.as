package
{
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.geom.Rectangle;
	import org.flintparticles.common.counters.TimePeriod;
	import org.flintparticles.common.displayObjects.Dot;
	import org.flintparticles.common.initializers.CollisionRadiusInit;
	import org.flintparticles.common.initializers.SharedImage;
	import org.flintparticles.twoD.actions.Accelerate;
	import org.flintparticles.twoD.actions.Collide;
	import org.flintparticles.twoD.actions.CollisionZone;
	import org.flintparticles.twoD.actions.DeathZone;
	import org.flintparticles.twoD.actions.Move;
	import org.flintparticles.twoD.emitters.Emitter2D;
	import org.flintparticles.twoD.initializers.Position;
	import org.flintparticles.twoD.initializers.Velocity;
	import org.flintparticles.twoD.zones.DiscZone;
	import org.flintparticles.twoD.zones.LineZone;
	import org.flintparticles.twoD.zones.PointZone;
	import org.flintparticles.twoD.zones.RectangleZone;
	import org.flintparticles.twoD.zones.Zone2D;
	
	import flash.geom.Point;
	
	/**
	 * ...
	 * @author Roman Komissarov
	 */
	public class BallsEmitter extends Emitter2D
	{
		
		public static const RECTANGLE_ZONE:String = "rectZone";
		public static const CIRCLE_ZONE:String = "circleZone";
		
		//public var good:Zone2D = new RectangleZone(300, 230, 340, 250);
		//public var evil:Zone2D = new PointZone( new Point( 320, 240 ) );
		//public var coll:CollisionZone = new CollisionZone(zone1, 10)
		public var zones:Array = new Array();
		
		public function BallsEmitter()
		{
			counter = new TimePeriod(500, 10);
			addInitializer(new SharedImage(new Dot(2, 0xFFC40E)));
			addInitializer(new CollisionRadiusInit(3));			
			
			addAction(new Move());
			addAction(new Accelerate(0, 100));
			addAction(new Collide());
			addAction(new DeathZone(new RectangleZone(0, 480, 640, 500)));
	
			//addAction(new CollisionZone(new DiscZone(new Point(320, 240), 242), 0.5));
			//addAction(new CollisionZone(good, 0.5));
		}
		
		public function addPin( x:Number, y:Number ):void
		{
		  addAction( new CollisionZone( new DiscZone( new Point( x, y ), 3 ), 0.5 ) );
		}
		
		public function addCollisionZone(type:String, params:Object):Zone2D {
			switch (type) 
			{
				case RECTANGLE_ZONE:
					var rectX:Number = params.x;
					var rectY:Number = params.y;
					var rectWidth:Number = params.width
					var rectHeight:Number = params.height;
					var bounce:Number = params.bounce
					var zone:Zone2D = new RectangleZone(rectX, rectY, rectX + rectWidth, rectY + rectHeight);
					zones.push(zone);
					addAction(new CollisionZone(zone, bounce));
					break;
					
				case CIRCLE_ZONE:
					var circleX:Number = params.x;
					var circleY:Number = params.y;
					var circleRad:Number = params.rad;
					var bounce:Number = params.bounce;
					var zone:Zone2D = new DiscZone(new Point(circleX, circleY), circleRad);
					zones.push(zone);
					addAction(new CollisionZone(zone, bounce));
					break;
				
				
				default:
					break;
				
			}
			
			return zone;
		}
		
		public function getCollisionParamsFromObject(type:String , obj:DisplayObject):Object {
			switch (type) 
			{
				case RECTANGLE_ZONE:
					var rect:Rectangle = obj.getBounds(obj.parent);
					return ({ "x":rect.x, "y":rect.y, "width":rect.width,"height":rect.height, "bounce":0.5 });
					break;
					
				case CIRCLE_ZONE:
					var global:Point = obj.parent.localToGlobal(new Point(obj.x, obj.y));
					return ({ "x":global.x, "y":global.y, "rad":Math.abs((obj.width * obj.parent.scaleX )/ 2), "bounce":0.5 });
					break;
				
				default:
					break;
				
			}
			return null;
		}
		
		public function setRespawnPosition(point1:Point, point2:Point):void {
			addInitializer(new Position(new LineZone(point1, point2)));
		}
		
		public function setVelocity(point:Point, rad:Number):void {
			addInitializer(new Velocity(new DiscZone(point, rad)));
		}
		
		public function defaultInit():void {
			addInitializer(new Position(new LineZone(new Point(640/2-10, 5), new Point(640/2+10, 5))));
			addInitializer(new Velocity(new DiscZone(new Point(0, 100), 20)));
		}
	
	}

}
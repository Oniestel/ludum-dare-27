package 
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;

	/**
	 * ...
	 * @author Roman Komissarov
	 */
	[Frame(factoryClass = "Preloader")]
	[SWF(width='640', height='480', frameRate='30', backgroundColor='#FFFFFF')]
	public class Main extends Sprite 
	{
		
		private var menu:DesignMenu;
		
		public function Main():void 
		{
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}

		private function init(e:Event = null):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			// entry point
			menu = new DesignMenu();
			addChild(menu);
			menu.btnStart.mouseEnabled = true;
			menu.btnStart.addEventListener(MouseEvent.CLICK, onStartClick);
			
		}
		
		private function onStartClick(e:MouseEvent):void 
		{
			removeChild(menu);
			addChild(new Game(stage));
		}

	}

}
package  
{
	import flash.display.DisplayObject;
	/**
	 * ...
	 * @author Roman Komissarov
	 */
	public class Utils 
	{
		
		public function Utils() 
		{
			
		}
		
		public static function getCircle(obj:DisplayObject):Number
		{
		// The formula is Pi times the radius squared.
			return Math.PI * Math.pow((obj.width / 2), 2);
		}
		
	}

}
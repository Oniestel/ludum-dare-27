package
{
	import flash.display.Stage;
	import flash.geom.Point;
	import levels.Level1;
	import org.flintparticles.common.events.EmitterEvent;
	import org.flintparticles.common.events.ParticleEvent;
	import org.flintparticles.common.particles.Particle;
	import org.flintparticles.twoD.renderers.BitmapRenderer;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	import com.greensock.TweenMax;
	
	/**
	 * ...
	 * @author Roman Komissarov
	 */
	public class Game extends Sprite
	{
		private var level:Level;
		private var gameMenu:DesignGameMenu;
		
		public function Game(stage:Stage)
		{
			level = new Level1();
			addChild(level);
			gameMenu = new DesignGameMenu();
			addChildAt(gameMenu,1);
			gameMenu.btn.visible = false;
			
			level.dropBalls();
			level.addEventListener("ballsEnd", onLevelFail);
			level.addEventListener("levelComplete", onLevelComplete);
			gameMenu.btn.addEventListener(MouseEvent.CLICK, onGameMenuBtn);
			addEventListener(MouseEvent.CLICK, onMouseClick);
		}
		
		private function onGameMenuBtn(e:MouseEvent):void 
		{

			TweenMax.to(gameMenu.btn, 0.5, { alpha:0, onComplete: function():void {
				gameMenu.btn.visible = false;
				removeChild(level);
				level.clean();
				level = null;
				level = new Level1();
				level.addEventListener("ballsEnd", onLevelFail);
				level.addEventListener("levelComplete", onLevelComplete);
				addChildAt(level, 0);
				level.alpha = 0;
				TweenMax.to(level, 0.5, { alpha:1, onComplete: function():void {
					level.dropBalls();
				}} );
				
			}} );
		}
		
		private function onLevelComplete(e:Event):void 
		{
			level.removeEventListener("ballsEnd", onLevelFail);
			level.removeEventListener("levelComplete", onLevelComplete);
			TweenMax.to(level, 0.5, { alpha:0 } );
			gameMenu.btn.visible = true;
			gameMenu.btn.alpha = 0;
			TweenMax.to(gameMenu.btn, 0.5, { alpha:1 } );
			gameMenu.btn.text = "You win! Play again?";
		
		}
		
		private function onLevelFail(e:Event):void 
		{
			level.removeEventListener("ballsEnd", onLevelFail);
			level.removeEventListener("levelComplete", onLevelComplete);
			TweenMax.to(level, 0.5, { alpha:0 } );
			gameMenu.btn.visible = true;
			gameMenu.btn.alpha = 0;
			TweenMax.to(gameMenu.btn, 0.5, { alpha:1 } );
			gameMenu.btn.text = "You lose! Retry?";
		}
		
		private function onMouseClick(e:MouseEvent):void 
		{
			level.addPin(e.stageX, e.stageY);
		}
		
	
	}

}
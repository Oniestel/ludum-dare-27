package levels 
{
	import flash.display.FrameLabel;
	import flash.display.MovieClip;
	import flash.display.Scene;
	import flash.events.Event;
	import flash.events.TimerEvent;
	import org.flintparticles.common.events.EmitterEvent;
	import org.flintparticles.common.events.ParticleEvent;
	import com.greensock.TweenMax;
	
	/**
	 * ...
	 * @author Roman Komissarov
	 */
	public class Level1 extends Level
	{
		
		//private var design:DesignLevel1 = new DesignLevel1();
		
		public function Level1() 
		{
			super();
			emitter.defaultInit();
			//addChild(design);
			//addCollisionCircle(design.collBirdBody);
			//addCreatureWithCompleteCircle(design.bird1, design.collBird);
			//addCollisionRectangle(design.collRock);
			generateCreatures();
			initCreatures();
			addEventListener(Event.ENTER_FRAME, onEnterFrame)
			
		}
		
		override public function clean():void 
		{
			super.clean();
			removeEventListener(Event.ENTER_FRAME, onEnterFrame)
		}
		
		private function initCreatures():void {
			for (var i:int = 0; i < creatures.length; i++) 
			{
				creatures[i].creature.gotoAndStop("openMounth");
			}
		}
		
		private function onEnterFrame(e:Event):void 
		{
			for (var i:int = 0; i < creatures.length; i++) 
			{
				if (creatures[i].creature.currentFrameLabel == "endWink") creatures[i].creature.gotoAndPlay("eat")
			}
		}
		
		private function creatureWink(mc:MovieClip):void {
			mc.gotoAndPlay("openMounth");
		}
		
		private function creatureSleep(mc:MovieClip):void {
			mc.gotoAndStop("sleep");
		}		
		
		
		//Start featurecut!
		
		
		private function generateCreatures():void {
			
			for (var i:int = 1; i < 4; i++) 
			{
				generateCreature();
			}
			
		}
		
		private function generateCreature():void {
			
			var mc:DesignBird = new DesignBird();
			addChild(mc);
			mc.scaleX = mc.scaleY = Math.random();
			if (mc.scaleX < 0.5) mc.scaleX = mc.scaleY = 0.5;
			//mc.scaleX = mc.scaleX * Math.random() > 0.5 ? 1 : -1;
			mc.x = Math.random() * (640);
			mc.y = Math.random() * (480);
			if (mc.x < 150) mc.x = 150;
			if (mc.y < 150) mc.y = 150;
			if (mc.x + mc.width > 640) mc.x = 640 - mc.width;
			if (mc.y + mc.height > 480) mc.y = 480 - mc.height;
			if (Math.random() > 0.5) {
				mc.scaleX = mc.scaleX * -1;
			}
			addCollisionCircle(mc.collBirdBody);
			addCreatureWithCompleteCircle(mc.bird1, mc.collBird);
			//addCollisionRectangle(mc.collRect);
		}
		
		override protected function completeCreature(creature:Object):void 
		{
			
			creatureSleep(creature.creature);
			super.completeCreature(creature);
			super.completeCreature(creature);
			super.completeCreature(creature);
		}
		
		override protected function touchCreature(creature:Object):void 
		{
			creatureWink(creature.creature)
			super.touchCreature(creature);
		}
		
		override protected function onBallsEnd(e:TimerEvent):void 
		{
			super.onBallsEnd(e);
		}
		
		override protected function onZoneCollision(e:ParticleEvent):void 
		{
			super.onZoneCollision(e);
		}
		
	}

}
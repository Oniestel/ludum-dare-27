package  
{
	
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.Stage;
	import flash.events.TimerEvent;
	import flash.geom.Point;
	import flash.utils.Timer;
	import org.flintparticles.common.events.EmitterEvent;
	import org.flintparticles.common.events.ParticleEvent;
	import org.flintparticles.common.particles.Particle;
	import org.flintparticles.twoD.renderers.BitmapRenderer;
	import org.flintparticles.twoD.zones.Zone2D;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	/**
	 * ...
	 * @author Roman Komissarov
	 */
	public class Level extends Sprite
	{
		
		protected var emitter:BallsEmitter;
		protected var renderer:BitmapRenderer;
		protected var creatures:Array = new Array();
		
		protected var endTimer:Timer;
		
		public function Level() 
		{			
			emitter = new BallsEmitter();
			renderer = new BitmapRenderer(new Rectangle(0, 0, 640, 480));
			renderer.addEmitter(emitter);
			addChild(renderer);
			endTimer = new Timer(12000, 1);
			endTimer.addEventListener(TimerEvent.TIMER, onBallsEnd);
			//emitter.addEventListener(EmitterEvent.EMITTER_EMPTY, onBallsEnd);
			emitter.addEventListener(ParticleEvent.ZONE_COLLISION, onZoneCollision);
		}
		
		public function clean():void {
			endTimer.removeEventListener(TimerEvent.TIMER, onBallsEnd);
			//emitter.addEventListener(EmitterEvent.EMITTER_EMPTY, onBallsEnd);
			emitter.removeEventListener(ParticleEvent.ZONE_COLLISION, onZoneCollision);
		}
		
		public function addPin(x:Number, y:Number):void
		{
			graphics.beginFill(0x999999);
			graphics.drawCircle(x, y, 3);
			graphics.endFill();
			emitter.addPin(x, y);
		}
		
		public function dropBalls():void
		{
			if (!emitter.running)
			{
				emitter.start();
				endTimer.start();
			}
		}
		
		protected function onBallsEnd(e:TimerEvent):void {
			dispatchEvent(new Event("ballsEnd"));
		}

		protected function onZoneCollision(e:ParticleEvent):void 
		{
			for (var i:int = 0; i < creatures.length; i++) 
			{
				if (e.otherObject == creatures[i].zone && creatures[i].complete == false) {
					
					creatures[i].have++;
					if (creatures[i].have >= creatures[i].need) {
						completeCreature(creatures[i]);
					} else {
						touchCreature(creatures[i]);
					}
					e.particle.isDead = true;
				}
			}
		}		
		
		protected function touchCreature(creature:Object):void {
			
		}		
		
		protected function completeCreature(creature:Object):void {
			
			creature.complete = true;
			checkLevelComplete();
		}
		
		protected function checkLevelComplete():void 
		{
			for (var i:int = 0; i < creatures.length; i++) 
			{
				if (!creatures[i].complete) return;
			}
			dispatchEvent(new Event("levelComplete"));
			//dispatchLevelComplete
		}
		
		protected function addPins():void
		{
			var pins:Array = [241, 81, 222, 81, 213, 88, 208, 94, 202, 100, 198, 106, 195, 113, 187, 93, 182, 98, 178, 103, 175, 109, 173, 114, 172, 120, 171, 127, 149, 128, 149, 120, 147, 113, 145, 108, 142, 103, 137, 98, 132, 94, 127, 90, 121, 86, 242, 123, 238, 127, 234, 131, 230, 135, 226, 139, 214, 151, 132, 124, 114, 124, 96, 124, 123, 140, 105, 140, 87, 140, 114, 156, 96, 156, 78, 156, 123, 172, 105, 172, 87, 172, 69, 172, 114, 188, 96, 188, 78, 188, 60, 188, 105, 204, 87, 204, 69, 204, 96, 220, 78, 220, 135, 202, 130, 206, 126, 210, 122, 215, 118, 220, 115, 226, 113, 232, 111, 238, 20, 191, 24, 193, 28, 196, 32, 200, 36, 204, 39, 208, 42, 211, 44, 215, 47, 219, 50, 224, 52, 228, 54, 233, 56, 238, 151, 202, 157, 206, 162, 211, 166, 216, 169, 221, 172, 227, 174, 233, 175, 239, 196, 169, 232, 169, 250, 169, 169, 185, 187, 185, 205, 185, 223, 185, 241, 185, 178, 201, 196, 201, 232, 201, 250, 201, 187, 217, 241, 217, 250, 233, 143, 220, 134, 236, 152, 236, 143, 252, 55, 280, 73, 280, 91, 280, 109, 280, 127, 280, 46, 296, 64, 296, 82, 296, 100, 296, 118, 296, 136, 296, 55, 312, 73, 312, 91, 312, 109, 312, 127, 312, 64, 330, 82, 330, 100, 330, 118, 330, 136, 330, 164, 314, 164, 320, 164, 326, 164, 332, 164, 338, 164, 344, 164, 350, 163, 356, 161, 362, 158, 367, 154, 370, 149, 372, 186, 314, 208, 314, 186, 400, 208, 400, 176, 414, 218, 414, 186, 340, 186, 346, 186, 352, 186, 358, 185, 364, 183, 370, 180, 375, 176, 378, 171, 380, 208, 340, 208, 346, 208, 352, 208, 358, 209, 364, 211, 370, 214, 375, 218, 378, 223, 380, 223, 264, 241, 264, 232, 282, 250, 282, 241, 298, 232, 314, 250, 314, 241, 330, 232, 346, 250, 346, 241, 362, 250, 378,];
			
			for (var i:int = 0; i < pins.length; i += 2)
			{
				addPin(pins[i], pins[i + 1]);
				if (x < 250)
				{
					addPin(500 - pins[i], pins[i + 1]);
				}
			}
		}
		
		protected function addCollisionRectangle(obj:DisplayObject):void {
			emitter.addCollisionZone(BallsEmitter.RECTANGLE_ZONE, emitter.getCollisionParamsFromObject(BallsEmitter.RECTANGLE_ZONE, obj));
			obj.visible = false;
		}
		
		protected function addCollisionCircle(obj:DisplayObject):void {
			emitter.addCollisionZone(BallsEmitter.CIRCLE_ZONE, emitter.getCollisionParamsFromObject(BallsEmitter.CIRCLE_ZONE, obj));
			obj.visible = false;
		}	
		
		protected function addCreatureWithCompleteRectangle(creatureMc:MovieClip, obj:DisplayObject, need:uint=10):void {
			var zone:Zone2D = emitter.addCollisionZone(BallsEmitter.RECTANGLE_ZONE, emitter.getCollisionParamsFromObject(BallsEmitter.RECTANGLE_ZONE, obj));
			obj.visible = false;
			creatures.push({"creature":creatureMc, "zone":zone, "have":0, "need":need, "complete":false});
		}
		
		protected function addCreatureWithCompleteCircle(creatureMc:MovieClip, obj:DisplayObject, need:uint=10):void {
			var zone:Zone2D = emitter.addCollisionZone(BallsEmitter.CIRCLE_ZONE, emitter.getCollisionParamsFromObject(BallsEmitter.CIRCLE_ZONE, obj));
			obj.visible = false;
			creatures.push({"creature":creatureMc, "zone":zone, "have":0, "need":need, "complete":false});
		}		
		
	}

}